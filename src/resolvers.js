const Karyawan = require('./models/karyawan');
const Admin = require('./models/admin');
const Periode = require('./models/periode');
const Gajian = require('./models/gajian');
const jwt = require('jsonwebtoken');

const resolvers = {
    Query: {
        async Karyawans() {
            return await Karyawan.find();
        },
        async getKaryawan(_, {_id}){
            return await Karyawan.findById(_id);
        },
        async Login(_, {username, password}) {
            const admin = await Admin.findOne({ username: username });
            if (!admin){
                throw new Error('user does not exists!');
            }

            if (password !== admin.password){
                throw new Error('password is incorrect!');
            }

            const token = jwt.sign({ userId: admin._id, username: admin.username }, 'somesupersecretkey', {
                expiresIn: '1h'
            });

            return {
                userId: admin._id,
                token: token
            };
        },
        async Periodes() {
            return await Periode.find();
        },
        async PeriodesByDate() {
            const result = await Periode.find();
            return result.reverse();
        },
        async Gajians() {
            return await Gajian.find();
        }
    },
    Mutation: {
        async createKaryawan(_, {input}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Karyawan.create(input);
        },
        async updateKaryawan(_, {_id, input}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Karyawan.findOneAndUpdate( {_id}, input, {new: true} );
        },
        async deleteKaryawan(_, {_id}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Karyawan.findByIdAndRemove(_id);
        },
        async createAdmin(_, {input}) {
            return await Admin.create(input);
        },
        async createPeriode(_, {input}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Periode.create(input);
        },
        async createGajian(_, {input}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Gajian.create(input);
        },async updatePeriode(_, {_id, input}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Periode.findOneAndUpdate( {_id}, input, {new: true} );
        },async updateGajian(_, {_id, input}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Gajian.findOneAndUpdate( {_id}, input, {new: true} );
        },
        async deletePeriode(_, {_id}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Periode.findByIdAndRemove(_id);
        },
        async deleteGajian(_, {_id}, req) {
            // if (!req.isAuth) {
            //     throw new Error('Unauthenticated');
            // }

            return await Gajian.findByIdAndRemove(_id);
        }
    }
};

module.exports = {resolvers};