const {makeExecutableSchema} = require('graphql-tools');
const {resolvers} = require('./resolvers');

const typeDefs = `
    type Admin {
        _id: ID
        username: String!
        password: String!
    }
    
    type Karyawan {
        _id: ID
        nama: String!
        bagian: String!
        gaji_pokok: Int!
        uang_makan: Int
        transport: Int
        apresiasi: Int
    }
    
    type Periode {
        _id: ID
        startDate: String!
        endDate: String!
    }
    
    type Gajian {
        _id: ID
        id_karyawan: String!
        id_periode: String!
        jumlah_hari: Float!
        jam_lembur: Float!
        total_gaji: Int!
        tanggal_pembayaran: String!
    }
    
    type AuthData {
        userId: ID!
        token: String!
    }

    type Query {
        getKaryawan(_id: ID): Karyawan
        Karyawans: [Karyawan]
        Periodes: [Periode]
        PeriodesByDate: [Periode]
        Gajians: [Gajian]
        Login(username: String!, password: String!): AuthData!
    }
    
    input KaryawanInput {
        nama: String!
        bagian: String!
        gaji_pokok: Int!
        uang_makan: Int
        transport: Int
        apresiasi: Int
    }
    
    input PeriodeInput {
        startDate: String!
        endDate: String!
    }
    
    input GajianInput {
        id_karyawan: String!
        id_periode: String!
        jumlah_hari: Int!
        jam_lembur: Int!
        total_gaji: Int!
        tanggal_pembayaran: String!
    }
    
    input AdminInput {
        username: String!
        password: String!
    }
    
    type Mutation {
        createKaryawan(input: KaryawanInput): Karyawan
        createAdmin(input: AdminInput): Admin
        createGajian(input: GajianInput): Gajian
        createPeriode(input: PeriodeInput): Periode
        
        updateKaryawan(_id: ID!, input: KaryawanInput): Karyawan
        updateGajian(_id: ID!, input: GajianInput): Gajian
        updatePeriode(_id: ID!, input: PeriodeInput): Periode
        
        deleteGajian(_id: ID!): Gajian
        deletePeriode(_id: ID!): Periode
        deleteKaryawan(_id: ID!): Karyawan
    }
`;

// export default makeExecutableSchema({
//     typeDefs,
//     resolvers
// });

module.exports = new makeExecutableSchema({ typeDefs, resolvers });