const express = require('express');
// import bodyParser from 'body-parser';
// import serverLess from 'serverless-http';
const GraphqlHTTP = require('express-graphql');
const schema = require('./schema');
const mongoose = require('mongoose');
const isAuth = require('./middleware/is-auth');
const { config } = require('./config');

const app = express();
const port = process.env.PORT || 3003;

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});

app.use(isAuth);

app.use('/graphql', GraphqlHTTP({
    graphiql: false,
    schema
}));

// app.get('/', (req, res) => {
//     res.json({
//         'hello' : 'test'
//     })
// });

mongoose.Promise = global.Promise;
mongoose.connect(
    config.db_uri,
    { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false }
).then(() => {
    app.listen(port, () => {
        console.log("server is running");
    });
    console.log('connected to database yeaah!!');
}).catch(err => {
    console.log('errornya kenapa? '+err);
});
