const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GajianSchema = new Schema({
    id_karyawan: {
        type: String,
        required: true
    },
    id_periode: {
        type: String,
        required: true
    },
    jumlah_hari: {
        type: Number,
        required: true
    },
    jam_lembur: {
        type: Number,
        required: true
    },
    total_gaji: {
        type: Number,
        required: true
    },
    tanggal_pembayaran: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Gajian', GajianSchema);