const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const KaryawanSchema = new Schema({
    nama: {
        type: String,
        required: true
    },
    bagian: {
        type: String,
        required: true
    },
    gaji_pokok: {
        type: Number,
        required: true
    },
    uang_makan: {
        type: Number
    },
    transport: {
        type: Number
    },
    apresiasi: {
        type: Number
    }
});

module.exports = mongoose.model('Karyawan', KaryawanSchema);