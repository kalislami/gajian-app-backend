const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PeriodeSchema = new Schema({
    startDate: {
        type: String,
        required: true
    },
    endDate: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Periode', PeriodeSchema);